<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>
        Lab#3
    </title>
    <meta name="keywords" content="homework,lab,html,css">
    <meta name="description" content="#3">
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div id="wrap_container">
    <div id="container">
        <div id="logo">

        </div>

        <div id="name">
            SmartDev
        </div>
    </div>

    <div id="navi">
        <div id="l_chapter">
            <a href="#">Home</a>
        </div>

        <div class="chapter">
            <a href="#">News</a>
        </div>

        <div class="chapter">
            <a href="#">About</a>
        </div>

        <div class="chapter">
            <a href="#">Contact</a>
        </div>

    </div>

    <div id="content_container">
        <?php
        require_once "./service_script/mysql_connect.php";
        $query = "SELECT title,content,id FROM post";
        $res = mysql_query($query);
        while ($row = mysql_fetch_array($res)) {
            echo '<H1 class="title">' . '<a href=./post/post.php?post=' . $row['id'] . '>' . $row['title'] . '</a>' . '</H1>';
            $temp_string = $row['content'];

            if (strlen($row['content']) > 250) {
                $break_pos = strpos($row['content'], " ", 250); //detect first position of break after 250 symbols
                $temp_string = substr($row['content'], 0, $break_pos); //cut
            }

            echo '<div class="content">' . $temp_string . ' <a href=./post/post.php?post=' . $row['id'] . '>...</a>' . '</div>';
        }
        mysql_close($res);
        ?>

    </div>
    <div id="page-buffer">
    </div>

</div>
<div id="footer">
    Phone: +79083191859; e-mail:sofinan@rambler.ru
</div>
</body>
</html>
