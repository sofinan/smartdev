-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 12 2014 г., 18:44
-- Версия сервера: 5.5.23
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `task2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `content`, `user_id`, `post_id`) VALUES
(10, '			cccccccccccccccccccccccccccccc					', 8, 2),
(11, '		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat rhoncus dui ac mattis. Integer adipiscing vestibulum enim sit amet lacinia. Nunc ultricies vehicula cursus. In ante augue, venenatis quis nulla at, iaculis consectetur mauris. Aenean leo lectus, posuere eu placerat ut, sodales eu sapien. Sed nec velit sed lectus interdum consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec malesuada, velit ac scelerisque rutrum, velit orci pretium erat, ut consequat odio ligula non ligula. Donec suscipit elit nisi. Nullam elit nisi, congue elementum dolor porttitor, ullamcorper ultrices massa. In iaculis congue massa, eu porta libero scelerisque vitae. Sed eget tempor dui, ut malesuada ju						', 14, 2),
(12, '			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat rhoncus dui ac mattis. Integer adipiscing vestibulum enim sit amet lacinia. Nunc ultricies vehicula cursus. In ante augue, venenatis quis nulla at, iaculis consec			', 15, 2),
(13, '123							', 19, 2),
(14, 'inna@rambler.ru								', 20, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `user_id`) VALUES
(2, 'TITLE_4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat rhoncus dui ac mattis. Integer adipiscing vestibulum enim sit amet lacinia. Nunc ultricies vehicula cursus. In ante augue, venenatis quis nulla at, iaculis consectetur mauris. Aenean leo lectus, posuere eu placerat ut, sodales eu sapien. Sed nec velit sed lectus interdum consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec malesuada, velit ac scelerisque rutrum, velit orci pretium erat, ut consequat odio ligula non ligula. Donec suscipit elit nisi. Nullam elit nisi, congue elementum dolor porttitor, ullamcorper ultrices massa. In iaculis congue massa, eu porta libero scelerisque vitae. Sed eget tempor dui, ut malesuada justo. ', 2),
(5, 'TITLE_1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat rhoncus dui ac mattis. Integer adipiscing vestibulum enim sit amet lacinia. Nunc ultricies vehicula cursus. In ante augue, venenatis quis nulla at, iaculis consectetur mauris. Aenean leo lectus, posuere eu placerat ut, sodales eu sapien. Sed nec velit sed lectus interdum consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec malesuada, velit ac scelerisque rutrum, velit orci pretium erat, ut consequat odio ligula non ligula. Donec suscipit elit nisi. Nullam elit nisi, congue elementum dolor porttitor, ullamcorper ultrices massa. In iaculis congue massa, eu porta libero scelerisque vitae. Sed eget tempor dui, ut malesuada justo. ', 7),
(10, 'TITLE_2', '			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat rhoncus dui ac mattis. Integer adipiscing vestibulum enim sit amet lacinia. Nunc ultricies vehicula cursus. In ante augue, venenatis quis nulla at, iaculis consectetur mauris. Aenean leo lectus, posuere eu placerat ut, sodales eu sapien. Sed nec velit sed lectus interdum consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec malesuada, velit ac scelerisque rutrum, velit orci pretium erat, ut consequat odio ligula non ligula. Donec suscipit elit nisi. Nullam elit nisi, congue elementum dolor porttitor, ullamcorper ultrices massa. In iaculis congue massa, eu porta libero scelerisque vitae. Sed eget tempor dui, ut malesuada justo. 					', 13),
(11, 'title_3', 'Little article						', 16),
(12, 'TITLE_5', '	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat rhoncus dui ac mattis. Integer adipiscing vestibulum enim sit amet lacinia. Nunc ultricies vehicula cursus. In ante augue, venenatis quis nulla at, iaculis consectetur mauris. Aenean leo lectus, posuere eu placerat ut, sodales eu sapien. Sed nec velit sed lectus interdum consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec malesuada, velit ac scelerisque rutrum, velit orci pretium erat, ut consequat odio ligula non ligula. Donec suscipit elit nisi. Nullam elit nisi, congue elementum dolor porttitor, ullamcorper ultrices massa. In iaculis congue massa, eu porta libero scelerisque vitae. Sed eget tempor dui, ut malesuada justo.							', 17),
(14, 'title_6', 'title6						', 21);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `user_name` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `user_name`) VALUES
(1, 'VASY@GMAIL.COM', 'VASY'),
(2, 'OLEG@GMAIL.COM', 'OLEG'),
(4, 'KOSTY@GMAIL.COM', 'KOSTY'),
(5, 'ANDREY@GMAIL.COM', 'ANDREY'),
(6, 'MIKHAIL@GMAIL.COM', 'MIKHAIL'),
(7, 'ANNA@GMAIL.COM', 'ANNA'),
(8, 'VALYA@GMAIL.COM', 'VALYA'),
(11, 'ANNA2@GMAIL.COM', 'ANNA2@GMAIL.COM'),
(13, 'EGOR@GMAIL.COM', 'EGOR@GMAIL.COM'),
(14, 'vanya@gmail.com', 'vanya@gmail.com'),
(15, 'ANNA3@GMAIL.COM', 'ANNA3@GMAIL.COM'),
(16, 'SVETA@GMAIL.COM', 'SVETA@GMAIL.COM'),
(17, 'NIK@GMAIL.COM', 'NIK@GMAIL.COM'),
(18, 'and@GMAIL.COM', 'and@GMAIL.COM'),
(19, 'ram@ram.ru', 'ram@ram.ru'),
(20, 'inna@rambler.ru', 'inna@rambler.ru'),
(21, 'sofinan@rambler.ru', 'sofinan@rambler.ru');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `comments_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `comments_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_6` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
